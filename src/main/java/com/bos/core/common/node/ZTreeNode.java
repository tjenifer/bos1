package com.bos.core.common.node;

import lombok.Data;

/**
 * jquery ztree 插件的节点
 */
@Data
public class ZTreeNode {

    private Long id;          //节点id

    private Long pId;         //父节点id

    private String name;      //节点名称

    private Boolean open;     //是否打开节点

    private Boolean checked;  //是否被选中

    /**
     * 创建ztree的父级节点
     */
    public static ZTreeNode createParent() {
        ZTreeNode zTreeNode = new ZTreeNode();
        zTreeNode.setChecked(true);
        zTreeNode.setId(0L);
        zTreeNode.setName("顶级");
        zTreeNode.setOpen(true);
        zTreeNode.setPId(0L);
        return zTreeNode;
    }
}
