
package com.bos.core.common.constant;

/**
 * 多数据源的枚举
 */
public interface DatasourceEnum {

    /**
     * bos数据源
     */
    String DATA_SOURCE_GUNS = "dataSourcebos";

    /**
     * 其他业务的数据源（第二个数据库）
     */
    String DATA_SOURCE_BIZ = "dataSourceBiz";

}
