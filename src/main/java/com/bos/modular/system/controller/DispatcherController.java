package com.bos.modular.system.controller;

import cn.stylefeng.roses.core.base.controller.BaseController;
import cn.stylefeng.roses.core.reqres.response.ResponseData;
import com.bos.core.shiro.ShiroKit;
import com.bos.modular.system.entity.User;
import com.bos.modular.system.service.DispathcherService;
import com.bos.modular.system.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.bos.modular.system.entity.dispatcher;

import java.util.List;
import java.util.Map;

/**
 * 取派员设置
 */
@Controller
@RequestMapping("/dispatcher")
public class DispatcherController extends BaseController {
    private String PREFIX="/system/basic/dispatcher/";

    @Autowired
    private DispathcherService dis;

    @Autowired
    private UserService userService;
    //跳转到取派员设置界面
    @RequestMapping("/list")
    public String gotodispatcher(){return PREFIX+"dispatcher.html";}
    //跳转到取派员添加界面
    @RequestMapping("/disaddpage")
    public String disaddpage(){return PREFIX+"dispatcheradd.html";}

    @RequestMapping(value = "/listt")
    @ResponseBody
    public List<Map<String, Object>> list(@RequestParam(value = "dispatchername", required = false) String condition)
    {
        List<Map<String, Object>> list = this.dis.dislist(condition);
        return list;
    }

    @RequestMapping(value = "/select")
    @ResponseBody
    public List<Map<String, Object>> selectlist()
    {
        List<Map<String, Object>> selectlist = this.dis.selectlist();
        return selectlist;
    }

    //添加取派员
    @RequestMapping(value = "/adddis")
    @ResponseBody
    public ResponseData adddis(dispatcher dissss) {
        dispatcher dd=dissss;
        User user= userService.getByAccount(dissss.getAccount());
        String phone= user.getPhone();  //电话号码
        dissss.setPhonenumber(phone);
        dissss.setDept(user.getDeptId()); //部门

        String name= user.getName(); //派送员名字
        dissss.setDispatchername(name);
        dissss.setUseornot("使用");
        if(dissss.getHas_PDA().equals("1")){
            dissss.setHas_PDA("有");
        }
        else {
            dissss.setHas_PDA("无");
        }

        dis.adddis(dissss);
        return SUCCESS_TIP;
    }
    //下拉框（选择派送员）
    @RequestMapping(value = "/selectall")
    @ResponseBody
    public List<Map<String, Object>> selectall()
    {
        List<Map<String, Object>> selectlist = this.dis.selectall();
        return selectlist;
    }
    //删除取派员
    @RequestMapping(value = "/deldis")
    @ResponseBody
    public ResponseData adddis(@RequestParam(value = "condition", required = false) Long condition) {
        dis.deldis(condition);
        return SUCCESS_TIP;
    }
}
