package com.bos.modular.system.controller;

import cn.stylefeng.roses.core.base.controller.BaseController;
import cn.stylefeng.roses.core.reqres.response.ResponseData;
import com.alibaba.fastjson.JSONObject;
import com.bos.core.shiro.ShiroKit;
import com.bos.modular.system.entity.sign;
import com.bos.modular.system.service.SignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import java.util.UUID; //先导入
import java.util.List;
import java.util.Map;

import static jdk.nashorn.internal.objects.NativeError.getFileName;

/**
 * 签收
 */
@Controller
@RequestMapping("/signli")
public class SignController extends BaseController {
    private String PREFIX="/system/adeliver/";

    @Autowired
    private SignService sign;

    //跳转到签收数据界面
    @RequestMapping("/list")
    public String sendandreceive(){return PREFIX+"sign.html";}
    //跳转到签收界面
    @RequestMapping("/signadd")
    public String signadd(){return PREFIX+"signadd.html";}
    /**
     * 获取签收信息列表
     */
    @RequestMapping(value = "/listt")
    @ResponseBody
    public List<Map<String, Object>> list(@RequestParam(value = "orderid", required = false) String condition)
    {
        List<Map<String, Object>> list = this.sign.list(condition);
        return list;
    }

    @RequestMapping(value = "/signa")
    public void list(@RequestParam(value = "orderid", required = false) Long condition)
    {
        ShiroKit.getUserNotNull().setOrderid(condition);
    }

    //签收上传照片
    @RequestMapping(value = "/signadd",method = RequestMethod.POST)
    @ResponseBody
    public String upload(MultipartFile file, HttpServletRequest request, HttpSession session) throws IOException{
        Long orderid=ShiroKit.getUserNotNull().getOrderid();
//        Object SS=request.getParameter("orderid");
        JSONObject json = new JSONObject();
        // 原始名称
        String originalFilename = file.getOriginalFilename();
        if (file != null && originalFilename != null && originalFilename.length() > 0) {
            //获取后缀名
            String sufixName = originalFilename.substring(originalFilename.indexOf("."));
            //重命名
            String newName = UUID.randomUUID() + sufixName;
            //设置文件上传的目录
            String picPath="E:\\imgUpload\\";
            File filePath=new File(picPath);
            //如果保存文件的地址不存在，就先创建目录
            if(!filePath.exists()){
                filePath.mkdirs();
            }
            String liangliang=picPath+newName;
            // 新图片
            File newFile = new File(picPath+newName);
            // 将内存中的数据写入磁盘
            file.transferTo(newFile);

            com.bos.modular.system.entity.sign signupdata=new sign();
            signupdata.setOrderid(orderid);
            signupdata.setImagepath(liangliang);
            sign.updatesign(signupdata);

            json.put("code", 0);
            json.put("face", newName);
            System.out.println(json.toString());
            return json.toString();
        }
        json.put("code", 1);
        System.out.println(json.toString());
        return json.toString();
    }



}
