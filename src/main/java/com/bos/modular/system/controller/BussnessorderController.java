package com.bos.modular.system.controller;

import cn.stylefeng.roses.core.base.controller.BaseController;
import cn.stylefeng.roses.core.reqres.response.ResponseData;
import com.bos.core.common.annotion.BussinessLog;
import com.bos.core.common.annotion.Permission;
import com.bos.core.common.constant.dictmap.DeptDict;
import com.bos.core.shiro.ShiroKit;
import com.bos.modular.system.entity.BussnessOrder;

import com.bos.modular.system.entity.Logicstic;
import com.bos.modular.system.entity.OrderRoute;

import com.bos.modular.system.service.LogicsticService;
import com.bos.modular.system.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;
import java.util.UUID;

@Controller
@RequestMapping("/order")
public class BussnessorderController extends BaseController {
    private String PREFIX="/system/accept/bussnessorder/";
    @Autowired
    private OrderService orders;

    @Autowired
    private LogicsticService logicsticService;
    //跳转到跳转到接受订单界面
    @RequestMapping("/list")
    public String goorder(){return PREFIX+"bussnessorder.html";}
    //工作单查询
    @RequestMapping("/look")
    public String lookpage(){return PREFIX+"bussnessorderlook.html";}
    //审核订单页面
    @RequestMapping("/shenhepage")
    public String shenhepage(){return PREFIX+"bussnessordershenhe.html";}
    //分配路线页面
    @RequestMapping("/routepage")
    public String routepage(){return PREFIX+"routepage.html";}


    @RequestMapping(value = "/listt")
    @ResponseBody
    public List<Map<String, Object>> list(@RequestParam(value = "condition", required = false) Long condition)
    {
        List<Map<String, Object>> list = this.orders.list(condition);
        return list;
    }
//分配订单的路线
    @RequestMapping(value = "/giveroute")
    @ResponseBody
    public List<Map<String, Object>> giveroute(@RequestParam(value = "orderid", required = false) Long condition){
        List<Map<String, Object>> list = this.orders.list(condition);
        return list;

    }

    @RequestMapping(value = "/add")
    @ResponseBody
    public ResponseData update(BussnessOrder ord) {
//        BussnessOrder ss=ord;
        String toaddress=ord.getToprovince()+"-"+ord.getTocity()+"-"+ord.getToarea()+"-"+ord.getToaddressdescrript();
        String address=""+ord.getProvince()+"-"+ord.getCity()+"-"+ord.getArea()+"-"+ord.getAddressdescrript();
        ord.setAddress(address);
        ord.setToaddress(toaddress);
        //生成客户id
        String ss=UUID.randomUUID().toString();

        ss=ss.replace("-", "");//替换掉中间的那个斜杠

        ord.setCustomerid(ss);
        ord.setExamine("未审核");
        orders.inserorder(ord);
        return SUCCESS_TIP;
    }

    @RequestMapping(value = "/shenhe")
    @ResponseBody
    public ResponseData shenhe(@RequestParam(value = "orderid", required = false) Long orderid) {
        Long ss=orderid;
        orders.shenhe(orderid);
        return SUCCESS_TIP;
    }

    @RequestMapping(value = "/route_add")
    @ResponseBody
    public ResponseData sendadd(OrderRoute ru) {
        //用户id
//        Long userId = ShiroKit.getUserNotNull().getId();
//        //部门id
//        Long deptid=ShiroKit.getUserNotNull().getDeptId();
//        send.setDodeptid(deptid);
//        send.setOperatorid(userId);
//        sen.addSendandre(send);
//        Sendandre s=send;
//        OrderRoute oddd=ru;

        this.orders.addroute(ru);
        Long orderid=ru.getOrderid();
        orders.shenhe(orderid);
        Logicstic logicstic=new Logicstic();
        logicstic.setEventt("订单已打印");
        logicstic.setNowstate("打印订单");
        logicstic.setOrderid(orderid);
        logicsticService.addlogicstic(logicstic);
        return SUCCESS_TIP;
    }

    @RequestMapping(value = "/delorder")
    @ResponseBody
    public ResponseData delorder(@RequestParam(value = "condition", required = false) Long condition){
        orders.delorder(condition);
        return SUCCESS_TIP;
    }


}
