package com.bos.modular.system.controller;

import cn.stylefeng.roses.core.base.controller.BaseController;
import cn.stylefeng.roses.core.reqres.response.ResponseData;

import com.bos.core.shiro.ShiroKit;
import com.bos.core.shiro.ShiroUser;
import com.bos.modular.system.entity.Deliver;
import com.bos.modular.system.entity.Logicstic;
import com.bos.modular.system.entity.User;
import com.bos.modular.system.entity.inware;
import com.bos.modular.system.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

//入库操作
@Controller
@RequestMapping("/inware")
public class InwareController extends BaseController {
    private String PREFIX="/system/asorting/";

    @Autowired
    private InwareService inware;

    @Autowired
    private LogicsticService logicsticService;

    @Autowired
    private WareService wareService;

    @Autowired
    private UserService userService;

    @Autowired
    private DeliverService deliverService;
    //到入库界面
    @RequestMapping("/list")
    public String goregion(){return PREFIX+"inwarelist.html";}

    //到添加入库界面
    @RequestMapping("/add")
    public String inwareadd(){return PREFIX+"inware_add.html";}

//入库管理
    @RequestMapping(value = "/inware_add")
    @ResponseBody
    public ResponseData addinwarea(inware inware22) {
        Long userId = ShiroKit.getUserNotNull().getId();
        inware22.setOperaterid(userId);
        inware.addinware(inware22);

        Logicstic logicstic=new Logicstic();

        Long wareid=inware22.getWarehouseid();
        List<Map<String, Object>> tt=wareService.listbyid(wareid);
        Map<String,Object> lili=tt.get(0);
        Object mcm=lili.get("warename");

        String warename=(String) mcm;
        logicstic.setOrderid(inware22.getOrderid());
        String ruku="当前已到【"+mcm+"】";
        logicstic.setEventt(ruku);
        logicstic.setNowstate("入库");
        logicsticService.addlogicstic(logicstic);
//        Sendandre s=send;
        return SUCCESS_TIP;
    }

    /**
     * 获取所有入库列表
     */
    @RequestMapping(value = "/listt")
    @ResponseBody
    public List<Map<String, Object>> list(@RequestParam(value = "simplecode", required = false) String condition)
    {
        List<Map<String, Object>> list = this.inware.list(condition);
        List<Map<String, Object>> listkk= new ArrayList<>();
        int count=list.size();
        for(int j=0;j<count;j++) {
            Map<String, Object> ss = list.get(j);
            Object warehouseid = ss.get("warehouseid");
            if (warehouseid.equals(ss.get("sendware"))) {
                ss.put("nextware", ss.get("mwaname"));
            } else if (warehouseid.equals(ss.get("mware"))) {
                ss.put("nextware", ss.get("lastname"));
            } else {   //会将该订单分配给派送员，进入派送管理界面
                ss.put("nextware", "派送");
            }
            listkk.add(ss);
        }


        return listkk;
    }


//出库管理
    @RequestMapping(value = "/outwareudata")
    @ResponseBody
    public ResponseData outwareudata(inware waredd) {
        waredd.setOperaterid(ShiroKit.getUserNotNull().getId());
        //根据id删除入库表该订单的信息
        inware.outwareudata(waredd.getId());
        //再出库表中备份该信息
        inware.outinsert(waredd);
        //出库的时候判断是否是终点站，如果是终点站的话下一步就是派送了
        Logicstic logicstic=new Logicstic();
        String ss="【"+waredd.getWare()+"】"+"已出库";
        logicstic.setEventt(ss);
        String gg="";

//        ShiroUser shiroUser=ShiroKit.getUserNotNull();
        //获取到当前出库人的姓名
        String username=ShiroKit.getUserNotNull().getName();
        //获取到当前操作人的账号
        String account=ShiroKit.getUserNotNull().getAccount();
        //通过账号获取实体类user封装的基本信息
        User user= userService.getByAccount(account);
        //获取操作人的电话号码
        String phone=user.getPhone();
        //如果下一站是派送的话
        //最后一站出库的话还需要将数据传递给派送表
        if(waredd.getNextware().equals("派送")){
             gg = "【" + username + "】派送中，联系电话【"+ phone+"】";
            Deliver deliver=new Deliver();
            deliver.setOrderid(waredd.getOrderid());
            deliver.setDispacherid(ShiroKit.getUserNotNull().getId());
             deliverService.add(deliver);
        }
        else {
             gg = "即将发往【" + waredd.getNextware() + "】";
        }
        logicstic.setNextdo(gg);
        logicstic.setOrderid(waredd.getOrderid());
        logicstic.setNowstate("出库");
        //合成物流信息
        logicsticService.addlogicstic(logicstic);


        return SUCCESS_TIP;
    }


}
