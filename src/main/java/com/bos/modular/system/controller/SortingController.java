package com.bos.modular.system.controller;

import cn.stylefeng.roses.core.base.controller.BaseController;
import cn.stylefeng.roses.core.reqres.response.ResponseData;
import com.bos.core.shiro.ShiroKit;
import com.bos.modular.system.entity.WarehHouse;
import com.bos.modular.system.service.WareService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/sort")
public class SortingController extends BaseController {
    private String PREFIX="/system/asorting/";

    @Autowired
    private WareService ware;

    //跳转到仓库管理界面
    @RequestMapping("/warehouse")
    public String sendandreceive(){return PREFIX+"warehouse.html";}

    //跳转到仓库添加界面
    @RequestMapping("/addpage")
    public String addwarepage(){return PREFIX+"warehouseadd.html";}

    /**
     * 获取所有仓库列表
     */
    @RequestMapping(value = "/listt")
    @ResponseBody
    public List<Map<String, Object>> list(@RequestParam(value = "warename", required = false) String condition)
    {
        List<Map<String, Object>> list = this.ware.list(condition);
        return list;
    }
    //添加仓库
    @RequestMapping(value = "/add")
    @ResponseBody
    public ResponseData sendadd(WarehHouse warehHouse) {
        ware.addware(warehHouse);
        return SUCCESS_TIP;
    }
    @RequestMapping(value = "/delware")
    @ResponseBody
    public ResponseData delware(@RequestParam(value = "condition", required = false) Long condition){
        ware.delware(condition);
        return SUCCESS_TIP;
    }
}
