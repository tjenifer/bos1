package com.bos.modular.system.controller;

import cn.stylefeng.roses.core.base.controller.BaseController;
import cn.stylefeng.roses.core.reqres.response.ResponseData;
import com.bos.core.shiro.ShiroKit;
import com.bos.modular.system.entity.Deliver;
import com.bos.modular.system.entity.Logicstic;
import com.bos.modular.system.entity.OrderRoute;
import com.bos.modular.system.entity.sign;
import com.bos.modular.system.service.DeliverService;
import com.bos.modular.system.service.LogicsticService;
import com.bos.modular.system.service.SignService;
import com.bos.modular.system.service.sendandreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 派送管理
 */
@Controller
@RequestMapping("/deliver")
public class DeliverController extends BaseController{
    private String PREFIX="/system/adeliver/";

    @Autowired
    private DeliverService deliverService;
    @Autowired
    private SignService signService;

    @Autowired
    private LogicsticService logicsticService;
    //跳转到派送管理界面
    @RequestMapping("/list")
    public String sendandreceive(){return PREFIX+"deliver.html";}
    //跳转到调度界面
    @RequestMapping("/change")
    public String change(){return PREFIX+"change.html";}
    /**
     * 获取所有派送任务列表
     */
    @RequestMapping(value = "/listt")
    @ResponseBody
    public List<Map<String, Object>> list(@RequestParam(value = "orderid", required = false) Long condition)
    {
        List<Map<String, Object>> list = this.deliverService.list(condition);
        return list;
    }

    //签收
    @RequestMapping(value = "/sign")
    @ResponseBody
    public ResponseData signname(Deliver deliver) {
        //我先暂存下订单号
//        ShiroKit.getUserNotNull().setOrderid(deliver.getOrderid());
        //派送表中删除数据
        deliverService.deldispa(deliver.getId());
        //签收表数据中插入数据
        sign signccc=new sign();
        signccc.setOrderid(deliver.getOrderid());
        signccc.setUserid(deliver.getDispacherid());
        signccc.setDelivername(deliver.getDelivername());
        signccc.setDeliverphone(deliver.getDeliverphone());
        signService.addsign(signccc);
        //记录物流事件
        Logicstic logicstic=new Logicstic();
        logicstic.setOrderid(deliver.getOrderid());
        logicstic.setEventt("快件已签收，为本人签收");
        logicstic.setNextdo("感谢您在使用本物流，欢迎您再次光临！");
        logicstic.setNowstate("签收");
        logicsticService.addlogicstic(logicstic);


        return SUCCESS_TIP;
    }
    //调度
    @RequestMapping(value = "/changeadd")
    @ResponseBody
    public ResponseData changeadd(Deliver deliver) {

        Long dispacid=deliver.getDispacherid();
        List<Map<String, Object>> list=deliverService.selectbyid(dispacid);
        Map<String, Object> SS=list.get(0);
        Long liangliang= (Long)SS.get("USER_ID");
        deliver.setDispacherid(liangliang);
        deliverService.changeadd(deliver);

        //更改物流信息
        Long condition =deliver.getOrderid();
        List<Map<String, Object>> list1=this.deliverService.list(condition);
        List<Map<String, Object>> list2=logicsticService.list(condition);
        Map<String, Object> ss=list1.get(0);
        String delivername=(String) ss.get("delivername");
        String phone=(String)ss.get("deliverphone");
        String nextdo="【"+delivername+"】派送中，联系电话【"+phone+"】";
        Logicstic logicstic=new Logicstic();
        logicstic.setNextdo(nextdo);
        Map<String, Object> liangliang1=list2.get(0);
        int id=(int) liangliang1.get("id");
        Long dd=Long.valueOf(id);
        logicstic.setId(dd);
        logicsticService.diaoupdata(logicstic);
        return SUCCESS_TIP;
    }



}
