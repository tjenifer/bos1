package com.bos.modular.system.controller;

import cn.stylefeng.roses.core.base.controller.BaseController;
import com.bos.modular.system.service.OutwareService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

//出库操作
@Controller
@RequestMapping("/outware")
public class OutwareController  extends BaseController {
    private String PREFIX="/system/asorting/";

    @Autowired
    private OutwareService outwae;
    //到出库界面
    @RequestMapping("/outwarepagegg")
    public String outwarepage(){return PREFIX+"outwarelist.html";}

    /**
     * 获取所有出库列表
     */
    @RequestMapping(value = "/outwarelists")
    @ResponseBody
    public List<Map<String, Object>> outwarelist(@RequestParam(value = "simplecode", required = false) String condition)
    {
        List<Map<String, Object>> list = this.outwae.list(condition);

        return list;
    }

}
