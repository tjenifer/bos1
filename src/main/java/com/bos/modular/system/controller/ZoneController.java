package com.bos.modular.system.controller;

import cn.stylefeng.roses.core.base.controller.BaseController;
import cn.stylefeng.roses.core.reqres.response.ResponseData;
import com.bos.modular.system.entity.Zone;
import com.bos.modular.system.service.ZoneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/Zone")
public class ZoneController extends BaseController {
    private String PREFIX="/system/basic/Zone/";

    @Autowired
    private ZoneService zone;
    //跳转到区域设置界面
    @RequestMapping("/list")
    public String goregion(){return PREFIX+"Zone.html";}

    @RequestMapping("/addpage")
    public String addpage(){
        return PREFIX+"Zoneadd.html";
    }
    /**
     * 获取所有区域设置列表
     */
    @RequestMapping(value = "/listt")
    @ResponseBody
    public List<Map<String, Object>> list(@RequestParam(value = "keyword", required = false) String condition)
    {
        List<Map<String, Object>> list = this.zone.list(condition);
        return list;
    }


    @RequestMapping(value = "/Zone_add")
    @ResponseBody
    public ResponseData regionadd(Zone zone1) {

        zone.addzone(zone1);
        return SUCCESS_TIP;
    }
    @RequestMapping(value = "/delzone")
    @ResponseBody
    public ResponseData delzone(@RequestParam(value = "condition", required = false) Long condition){
        zone.delzone(condition);
        return SUCCESS_TIP;
    }
}
