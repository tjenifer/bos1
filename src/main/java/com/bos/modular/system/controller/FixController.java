package com.bos.modular.system.controller;

import cn.stylefeng.roses.core.base.controller.BaseController;
import cn.stylefeng.roses.core.reqres.response.ResponseData;
import com.bos.modular.system.entity.Fix;
import com.bos.modular.system.entity.User;
import com.bos.modular.system.entity.dispatcher;
import com.bos.modular.system.service.FixService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import java.lang.String.*;

import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/Fix")
public class FixController extends BaseController {
    private String PREFIX="/system/basic/Fix/";

    @Autowired
    private FixService fix;
    @RequestMapping("/list")
    public String goregion(){return PREFIX+"Fix.html";}

    //跳转到定区添加界面
    @RequestMapping("/fixaddpage")
    public String add(){return PREFIX+"Fixadd.html";}

    /**
     * 获取所有区域设置列表
     */
    @RequestMapping(value = "/listt")
    @ResponseBody
    public List<Map<String, Object>> list(@RequestParam(value = "condition", required = false) String condition)
    {
        List<Map<String, Object>> list = this.fix.list(condition);
        return list;
    }

    //添加取派员
    @RequestMapping(value = "/Fixadd")
    @ResponseBody
    public ResponseData adddis(Fix fixx) {
        fix.addfix(fixx);
//        Fix SSS=fixx;
//        String str = fixx.getZone();

        //这是还原
//        String[] lllo=str.split("|");

        return SUCCESS_TIP;
    }
    @RequestMapping(value = "/delfix")
    @ResponseBody
    public ResponseData delfix(@RequestParam(value = "condition", required = false) Long condition){
        fix.delfix(condition);
        return SUCCESS_TIP;
    }

}
