package com.bos.modular.system.controller;

import cn.stylefeng.roses.core.base.controller.BaseController;
import cn.stylefeng.roses.core.reqres.response.ResponseData;
import com.bos.core.common.annotion.Permission;
import com.bos.core.shiro.ShiroKit;
import com.bos.modular.system.entity.Logicstic;
import com.bos.modular.system.entity.Sendandre;
import com.bos.modular.system.service.LogicsticService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

//记录物流状态的控制器
@Controller
@RequestMapping("/logistic")
public class LogisticController extends BaseController {
    private String PREFIX="/system/alogicstic/";

    @Autowired
    private LogicsticService logsi;

    //跳转到收派标准界面
    @RequestMapping("/list")
    public String sendandreceive(){return PREFIX+"logicstic.html";}

    /**
     * 获取所有物流列表
     */
    @RequestMapping(value = "/listt")
    @ResponseBody
    public List<Map<String, Object>> list(@RequestParam(value = "orderid", required = false) Long condition)
    {
        List<Map<String, Object>> list = this.logsi.list(condition);
        return list;
    }
    //添加物流信息
    @RequestMapping(value = "/stander_add")
    @Permission
    @ResponseBody
    public ResponseData addlogicstic(Logicstic logicstic) {
        //用户id
        Long userId = ShiroKit.getUserNotNull().getId();

        logicstic.setOperatorid(userId);
        logsi.addlogicstic(logicstic);
        return SUCCESS_TIP;
    }

    @RequestMapping(value = "/dellogic")
    @ResponseBody
    public ResponseData dellogic(@RequestParam(value = "condition", required = false) Long condition){
        logsi.dellogic(condition);
        return SUCCESS_TIP;
    }
}
