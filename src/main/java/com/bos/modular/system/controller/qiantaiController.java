package com.bos.modular.system.controller;

import cn.stylefeng.roses.core.base.controller.BaseController;

import com.bos.modular.system.service.LogicsticService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Controller
public class qiantaiController extends BaseController {
    @RequestMapping(value = "/qiantai2")
    public String qiantai2() {

        return "/qiantai.html";

    }
    @Autowired
    private LogicsticService logsi;

    @RequestMapping(value = "/wuliu")
    @ResponseBody
    public List<Map<String, Object>> wuliu(@RequestParam(value = "orderid", required = false) Long condition) {

        List<Map<String, Object>> list =logsi.list(condition);
        return list;
    }

}
