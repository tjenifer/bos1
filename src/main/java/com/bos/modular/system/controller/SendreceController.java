package com.bos.modular.system.controller;

import cn.stylefeng.roses.core.base.controller.BaseController;
import cn.stylefeng.roses.core.reqres.response.ResponseData;
import com.bos.core.common.annotion.BussinessLog;
import com.bos.core.common.annotion.Permission;

import com.bos.core.common.constant.dictmap.DeptDict;
import com.bos.core.common.constant.factory.ConstantFactory;
import com.bos.core.shiro.ShiroKit;
import com.bos.modular.system.entity.Dept;
import com.bos.modular.system.entity.Sendandre;
import com.bos.modular.system.service.sendandreService;
import org.apache.shiro.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Enumeration;
import java.util.List;
import java.util.Map;

/**
 * 收派标准
 */
@Controller
@RequestMapping("/sendandreceive")
public class SendreceController extends BaseController{
    private String PREFIX="/system/basic/sendandreceive/";

  @Autowired
  private sendandreService sen;

  //跳转到收派标准界面
    @RequestMapping("/list")
    public String sendandreceive(){return PREFIX+"sendandreceive.html";}

    /**
     * 获取所有收派标准列表
     */
    @RequestMapping(value = "/listt")
    @ResponseBody
    public List<Map<String, Object>> list(@RequestParam(value = "sendName", required = false) String condition)
    {
        List<Map<String, Object>> list = this.sen.list(condition);
        return list;
    }

//    @RequestMapping(value = "/stander_edit")
//    @ResponseBody
//    public List<Map<String, Object>> getbyid(@RequestParam(value = "sendid", required = false) Long sendid)
//    {
//        List<Map<String, Object>> list = this.sen.selectbyid(sendid);
//        return list;
//    }
    //
    @RequestMapping(value = "/sadd")
    public String goedit(){
        return PREFIX+"sendandre_add.html";
    }


    @RequestMapping(value = "/stander_edit")
    public String getbyid(@RequestParam(value = "sendid", required = false) Long sendid)
    {
        List<Map<String, Object>> list = this.sen.selectbyid(sendid);
        return PREFIX+"sendandre_edit.html";
    }
//修改收派
    @RequestMapping(value = "/sendupdata")
    @Permission
    @ResponseBody
    public ResponseData update(Sendandre send) {
        //用户id
        Long userId = ShiroKit.getUserNotNull().getId();
        //部门id
        Long deptid=ShiroKit.getUserNotNull().getDeptId();
        send.setDodeptid(deptid);
        send.setOperatorid(userId);
        sen.editSendandre(send);

        return SUCCESS_TIP;
    }
//添加收派
    @RequestMapping(value = "/stander_add")
    @Permission
    @ResponseBody
    public ResponseData sendadd(Sendandre send) {
        //用户id
        Long userId = ShiroKit.getUserNotNull().getId();
        //部门id
        Long deptid=ShiroKit.getUserNotNull().getDeptId();
        send.setDodeptid(deptid);
        send.setOperatorid(userId);
        sen.addSendandre(send);
//        Sendandre s=send;
        return SUCCESS_TIP;
    }

    @RequestMapping(value = "/delsend")

    @ResponseBody
    public ResponseData delsend(@RequestParam(value = "sendid", required = false) Long condition) {

        sen.delsend(condition);
        return SUCCESS_TIP;
    }

}
