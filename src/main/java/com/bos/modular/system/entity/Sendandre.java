package com.bos.modular.system.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;
/**
 * <p>
 * 收派标准表
 * </p>
 */
@TableName("basic_sendandre")
public class Sendandre implements Serializable{



        private static final long serialVersionUID = 1L;

        /**
         * 主键
         */
        @TableId(value = "id", type = IdType.ID_WORKER)
        private Long sendid;
        /**
         * 标准名
         */
        @TableField("sendname")
        private String sendname;
        /**
         * 最小重量
         */
        @TableField("minwe")
        private String minwe;
        /**
         * 最大重量
         */
        @TableField("maxwe")
        private String maxwe;
        /**
         * 创建时间
         */
        @TableField(value = "dotime", fill = FieldFill.INSERT)
        private Date dotime;
        /**
         * 创建人id
         */
        @TableField(value = "operator", fill = FieldFill.INSERT)
        private Long operatorid;
    /**
     * 操纵部门id
     */
    @TableField(value = "dodept")
    private Long dodeptid;
    /**
     * 部门名
     */
    @TableField(value = "DEPTNAME")
    private String deptname;
    /**
     * 创建人
     */
    @TableField(value = "opername")
    private String opeere;

    public String getDeptname() {
        return deptname;
    }

    public void setDeptname(String deptname) {
        this.deptname = deptname;
    }

    public String getOpeere() {
        return opeere;
    }

    public void setOpeere(String opeere) {
        this.opeere = opeere;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getSendid() {
        return sendid;
    }

    public void setSendid(Long sendid) {
        this.sendid = sendid;
    }

    public String getSendname() {
        return sendname;
    }

    public void setSendname(String sendname) {
        this.sendname = sendname;
    }

    public String getMinwe() {
        return minwe;
    }

    public void setMinwe(String minwe) {
        this.minwe = minwe;
    }

    public String getMaxwe() {
        return maxwe;
    }

    public void setMaxwe(String maxwe) {
        this.maxwe = maxwe;
    }

    public Date getDotime() {
        return dotime;
    }

    public void setDotime(Date dotime) {
        this.dotime = dotime;
    }

    public Long getOperatorid() {
        return operatorid;
    }

    public void setOperatorid(Long operatorid) {
        this.operatorid = operatorid;
    }

    public Long getDodeptid() {
        return dodeptid;
    }

    public void setDodeptid(Long dodeptid) {
        this.dodeptid = dodeptid;
    }



        @Override
        public String toString() {
            return "sendandre{" +
                    ", sendid=" + sendid +
                    ", sendname=" + sendname +
                    ", minwe=" + minwe +
                    ", maxwe=" + maxwe +
                    ", operatatorid=" + operatorid +
                    ", dotime=" + dotime +
                    ", dodeptid=" + dodeptid +
                    "}";
        }
}
