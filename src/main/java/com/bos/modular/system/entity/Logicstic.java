package com.bos.modular.system.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 物流状态表
 * </p>
 */
@TableName("logistics")
public class Logicstic implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.ID_WORKER)
    private Long id;
    /**
     * 订单号
     */
    @TableField("orderid")
    private Long orderid;
    /**
     * 事件名
     */
    @TableField("eventt")
    private String eventt;
    /**
     * 操作人
     */
    @TableField("operatorid")
    private Long operatorid;
    /**
     * 操作人姓名
     */
    @TableField("username")
    private String username;
    /**
     * 下一站
     */
    @TableField("nextdo")
    private String nextdo;
    /**
     * 当前物流状态
     */
    @TableField("nowstate")
    private String nowstate;
    /**
     * 操作时间
     */
    @TableField(value = "eventtime")
    private Date eventtime;

    public String getNowstate() {
        return nowstate;
    }

    public void setNowstate(String nowstate) {
        this.nowstate = nowstate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrderid() {
        return orderid;
    }

    public void setOrderid(Long orderid) {
        this.orderid = orderid;
    }

    public String getEventt() {
        return eventt;
    }

    public void setEventt(String eventt) {
        this.eventt = eventt;
    }

    public Long getOperatorid() {
        return operatorid;
    }

    public void setOperatorid(Long operatorid) {
        this.operatorid = operatorid;
    }

    public String getNextdo() {
        return nextdo;
    }

    public void setNextdo(String nextdo) {
        this.nextdo = nextdo;
    }

    public Date getEventtime() {
        return eventtime;
    }

    public void setEventtime(Date eventtime) {
        this.eventtime = eventtime;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
