package com.bos.modular.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.List;

@TableName("basic_fixedarea")
public class Fix implements Serializable {
    private static final long serialVersionUID = 1L;

    private String zone;

    /**
     * 主键
     */
    @TableId(value = "FIXAREAID", type = IdType.ID_WORKER)
    private Long fixareaid;
    /**
     * 定区名
     */
    @TableField("FIXNAME")
    private String fixname;

    private Long dispactcherid;

     /**
     * 取派员名字
     */
    @TableField("DISPATCHERNAME")
    private String dispactchername;
    /**
     * 取派员电话号码
     */
    @TableField("PHONENUMBER")
    private String phonenumber;


    /**
     * 部门名称
     */
    @TableField("FULL_NAME")
    private String deptname;


    public Long getDispactcherid() {
        return dispactcherid;
    }

    public void setDispactcherid(Long dispactcherid) {
        this.dispactcherid = dispactcherid;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getDeptname() {
        return deptname;
    }

    public void setDeptname(String deptname) {
        this.deptname = deptname;
    }



    public Long getFixareaid() {
        return fixareaid;
    }

    public void setFixareaid(Long fixareaid) {
        this.fixareaid = fixareaid;
    }

    public String getFixname() {
        return fixname;
    }

    public void setFixname(String fixname) {
        this.fixname = fixname;
    }

    public String getDispactchername() {
        return dispactchername;
    }

    public void setDispactchername(String dispactchername) {
        this.dispactchername = dispactchername;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    @Override
    public String toString() {
        return "Fix{" +
                ", id=" + fixareaid +
                ", fixname=" + fixname +
                ", dispactchername=" + dispactchername +
                ", phonenumber=" + phonenumber +
                ", deptname=" + deptname +

                "}";
    }


}
