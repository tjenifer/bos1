package com.bos.modular.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

@TableName("basic_zone")
public class Zone implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.ID_WORKER)
    private Long zoneid;
    /**
     * 区域id
     */
    @TableField("regionid")
    private Long regionid;

    /**
     * 省
     */
    @TableField("PROVINCE")
    private String provincee;
    /**
     * 市
     */
    @TableField("CITY")
    private String city;
    /**
     * 区
     */
    @TableField("AREA")
    private String area;
    /**
     * 分拣编码
     */
    @TableField("sortingcode")
    private String sortingcode;
    /**
     * 关键字
     */
    @TableField("keyword")
    private String keyword;
    /**
     * 起始号
     */
    @TableField("startingnumber")
    private String startingnumber;
    /**
     * 终止号
     */
    @TableField("endnumber")
    private String endnumber;
    /**
     * 单双号
     */
    @TableField("oddoreven")
    private String oddoreven;

    /**
     * 位置信息
     */
    @TableField("sposition")
    private String sposition;

    public Long getRegionid() {
        return regionid;
    }

    public void setRegionid(Long regionid) {
        this.regionid = regionid;
    }

    public Long getZoneid() {
        return zoneid;
    }

    public void setZoneid(Long zoneid) {
        this.zoneid = zoneid;
    }

    public String getProvincee() {
        return provincee;
    }

    public void setProvincee(String provincee) {
        this.provincee = provincee;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getSortingcode() {
        return sortingcode;
    }

    public void setSortingcode(String sortingcode) {
        this.sortingcode = sortingcode;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getStartingnumber() {
        return startingnumber;
    }

    public void setStartingnumber(String startingnumber) {
        this.startingnumber = startingnumber;
    }

    public String getEndnumber() {
        return endnumber;
    }

    public void setEndnumber(String endnumber) {
        this.endnumber = endnumber;
    }

    public String getOddoreven() {
        return oddoreven;
    }

    public void setOddoreven(String oddoreven) {
        this.oddoreven = oddoreven;
    }

    public String getSposition() {
        return sposition;
    }

    public void setSposition(String sposition) {
        this.sposition = sposition;
    }

    @Override
    public String toString() {
        return "Zone{" +
                ", id=" + zoneid +
                ", province=" + provincee +
                ", city=" + city +
                ", area=" + area +
                ", startingnumber=" + startingnumber +
                ", keyword=" + keyword +
                ", endnumber=" + endnumber +
                ", oddoreven=" + oddoreven +
                ", sposition=" + sposition +
                "}";
    }

}
