package com.bos.modular.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

@TableName("basic_sendandre")
public class Deliver implements Serializable {
    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.ID_WORKER)
    private Long id;
    /**
     * 订单号
     */
    @TableField("orderid")
    private Long orderid;
    /**
     * 派送员号码
     */
    @TableField("dispacherid")
    private Long dispacherid;
    /**
     * 签收人
     */
    private String signname;
    /**
     * 签收人号码
     */
    private String signphone;
    /**
     * 物品名
     */
    private String goodsname;
    /**
     * 派送员名字
     */
    private String delivername;
    /**
     * 派送员电话
     */
    private String deliverphone;
    /**
     * 派件地址
     */
    private String positions;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrderid() {
        return orderid;
    }

    public void setOrderid(Long orderid) {
        this.orderid = orderid;
    }

    public Long getDispacherid() {
        return dispacherid;
    }

    public void setDispacherid(Long dispacherid) {
        this.dispacherid = dispacherid;
    }

    public String getSignname() {
        return signname;
    }

    public void setSignname(String signname) {
        this.signname = signname;
    }

    public String getSignphone() {
        return signphone;
    }

    public void setSignphone(String signphone) {
        this.signphone = signphone;
    }

    public String getDelivername() {
        return delivername;
    }

    public void setDelivername(String delivername) {
        this.delivername = delivername;
    }

    public String getDeliverphone() {
        return deliverphone;
    }

    public void setDeliverphone(String deliverphone) {
        this.deliverphone = deliverphone;
    }

    public String getGoodsname() {
        return goodsname;
    }

    public void setGoodsname(String goodsname) {
        this.goodsname = goodsname;
    }

    public String getPositions() {
        return positions;
    }

    public void setPositions(String positions) {
        this.positions = positions;
    }
}
