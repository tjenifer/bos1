package com.bos.modular.system.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;

@TableName("basic_region")
public class Region implements Serializable {
    private static final long serialVersionUID = 1L;



    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.ID_WORKER)
    private Long regionid;
    /**
     * 省
     */
    @TableField("provincee")
    private String provincee;
    /**
     * 市
     */
    @TableField("city")
    private String city;
    /**
     * 区
     */
    @TableField("area")
    private String area;
    /**
     * 邮编
     */
    @TableField("zipcodee")
    private String zipcodee;
    /**
     * 简码
     */
    @TableField("simplecodee")
    private String simplecodee;

    public Long getRegionid() {
        return regionid;
    }

    public void setRegionid(Long regionid) {
        this.regionid = regionid;
    }

    public String getProvincee() {
        return provincee;
    }

    public void setProvincee(String provincee) {
        this.provincee = provincee;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getZipcodee() {
        return zipcodee;
    }

    public void setZipcodee(String zipcodee) {
        this.zipcodee = zipcodee;
    }

    public String getSimplecodee() {
        return simplecodee;
    }

    public void setSimplecodee(String simplecodee) {
        this.simplecodee = simplecodee;
    }

    public String getCitycodee() {
        return citycodee;
    }

    public void setCitycodee(String citycodee) {
        this.citycodee = citycodee;
    }

    /**
     * 城市编码
     */
    @TableField("citycodee")
    private String citycodee;

    @Override
    public String toString() {
        return "region{" +
                ", id=" + regionid +
                ", province=" + provincee +
                ", city=" + city +
                ", area=" + area +
                ", zipcode=" + zipcodee +
                ", simplecode=" + simplecodee +
                ", citycode=" + citycodee +
                "}";
    }

}
