package com.bos.modular.system.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;


@TableName("basic_dispatcher")
public class dispatcher implements Serializable {
    private static final long serialVersionUID = 1L;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getDispatcherid() {
        return dispatcherid;
    }

    public String getDispatchername() {
        return dispatchername;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public String getHas_PDA() {
        return has_PDA;
    }

    public String getUseornot() {
        return useornot;
    }

    public long getSendandre() {
        return sendandre;
    }

    public Long getDept() {
        return dept;
    }

    public void setDept(Long dept) {
        this.dept = dept;
    }

    public String getDeptname() {
        return deptname;
    }

    public void setDeptname(String deptname) {
        this.deptname = deptname;
    }

    /**
     * 主键
     */
    @TableId(value = "dispatcherid", type = IdType.ID_WORKER)
    private Long dispatcherid;
    /**
     * 取派员姓名
     */
    @TableField("dispatchername")
    private String dispatchername;
    /**
     * 标准id
     */
    @TableField("sendid")
    private String sendid;

    /**
     * 部门名
     */
    @TableField("deptname")
    private String deptname;
    /**
     * 取派员电话号码
     */
    @TableField("phonenumber")
    private String phonenumber;
    /**
     * 是否有掌上电脑
     */
    @TableField("has_PDA")
    private String has_PDA;

    /**
     * 是否作废
     */
    @TableField("useornot")
    private String useornot;
    /**
     * 取派标准
     */
    @TableField("sendandre")
    private long sendandre;

    /**
     * 取派标准名
     */
    @TableField("sendname")
    private String sendname;

    /**
     * 账号
     */
    @TableField("account")
    private String account;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getSendid() {
        return sendid;
    }

    public void setSendid(String sendid) {
        this.sendid = sendid;
    }

    public String getSendname() {
        return sendname;
    }

    public void setSendname(String sendname) {
        this.sendname = sendname;
    }

    public void setDispatcherid(Long dispatcherid) {
        this.dispatcherid = dispatcherid;
    }

    public void setDispatchername(String dispatchername) {
        this.dispatchername = dispatchername;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public void setHas_PDA(String has_PDA) {
        this.has_PDA = has_PDA;
    }

    public void setUseornot(String useornot) {
        this.useornot = useornot;
    }

    public void setSendandre(long sendandre) {
        this.sendandre = sendandre;
    }



    /**
     * 最大重量
     */
    @TableField("dept")
    private Long dept;
    @Override
    public String toString() {
        return "dispatcher{" +
                ", id=" + dispatcherid +
                ", dispatcher=" + dispatchername +
                ", phonenumber=" + phonenumber +
                ", has_PDA=" + has_PDA +
                ", useornot=" + useornot +
                ", sendandre=" + sendandre +
                ", dept=" + dept +
                "}";
    }
}
