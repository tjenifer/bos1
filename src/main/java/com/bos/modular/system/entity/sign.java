package com.bos.modular.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

@TableName("sign")
public class sign implements Serializable {
    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.ID_WORKER)
    private Long id;
    /**
     * 订单号
     */
    @TableField("orderid")
    private Long orderid;
    /**
     * 用户Id 在sys_user表中的
     */
    @TableField("userid")
    private Long userid;
    /**
     * 派件人姓名
     */
    @TableField("delivername")
    private String delivername;
    /**
     * 派件人号码
     */
    @TableField("deliverphone")
    private String deliverphone;

    @TableField("imagepath")
    private String imagepath;
    /**
     * 签收时间
     */
    @TableField("outtime")
    private Date outtime;

    public Date getOuttime() {
        return outtime;
    }

    public void setOuttime(Date outtime) {
        this.outtime = outtime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrderid() {
        return orderid;
    }

    public void setOrderid(Long orderid) {
        this.orderid = orderid;
    }

    public Long getUserid() {
        return userid;
    }

    public void setUserid(Long userid) {
        this.userid = userid;
    }

    public String getDelivername() {
        return delivername;
    }

    public void setDelivername(String delivername) {
        this.delivername = delivername;
    }

    public String getDeliverphone() {
        return deliverphone;
    }

    public void setDeliverphone(String deliverphone) {
        this.deliverphone = deliverphone;
    }

    public String getImagepath() {
        return imagepath;
    }

    public void setImagepath(String imagepath) {
        this.imagepath = imagepath;
    }
}
