package com.bos.modular.system.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.util.Date;

@TableName("orderroute")
public class OrderRoute {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.ID_WORKER)
    private Long id;
    /**
     * 标准名
     */
    @TableField("orderid")
    private Long orderid;
    /**
     * 最小重量
     */
    @TableField("sendware")
    private Long sendware;
    /**
     * 最大重量
     */
    @TableField("mware")
    private Long mware;
    /**
     * 最大重量
     */
    @TableField("lastware")
    private Long lastware;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrderid() {
        return orderid;
    }

    public void setOrderid(Long orderid) {
        this.orderid = orderid;
    }

    public Long getSendware() {
        return sendware;
    }

    public void setSendware(Long sendware) {
        this.sendware = sendware;
    }

    public Long getMware() {
        return mware;
    }

    public void setMware(Long mware) {
        this.mware = mware;
    }

    public Long getLastware() {
        return lastware;
    }

    public void setLastware(Long lastware) {
        this.lastware = lastware;
    }
}
