package com.bos.modular.system.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;

@TableName("sort_inware")
public class inware implements Serializable {
    private static final long serialVersionUID = 1L;



    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.ID_WORKER)
    private Long id;
    /**
     * 订单号
     */
    @TableField("orderid")
    private Long orderid;
    /**
     * 操作人id
     */
    @TableField("operaterid")
    private Long operaterid;
    /**
     * 仓库id
     */
    @TableField("warehouseid")
    private Long warehouseid;
    /**
     * 操作人
     */
    @TableField("usersname")
    private String usersname;
    /**
     * 仓库
     */
    @TableField("ware")
    private String ware;
    /**
     * 下一站仓库
     */
    @TableField("nextware")
    private String nextware;
    /**
     * 入库时间
     */
    @TableField("intotime")
    private Date intotime;

    /**
     * 出发仓库
     */

    private Long sendware;
    /**
     * 中转
     */

    private Long mware;
    /**
     * 到达
     */

    private Long lastware;
    /**
     * 初始名
     */

    private String sendname;
    /**
     * 中转名
     */

    private String mwaname;
    /**
     * 最终名
     */

    private String lastname;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrderid() {
        return orderid;
    }

    public void setOrderid(Long orderid) {
        this.orderid = orderid;
    }

    public Long getOperaterid() {
        return operaterid;
    }

    public void setOperaterid(Long operaterid) {
        this.operaterid = operaterid;
    }

    public Long getWarehouseid() {
        return warehouseid;
    }

    public void setWarehouseid(Long warehouseid) {
        this.warehouseid = warehouseid;
    }

    public String getUsersname() {
        return usersname;
    }

    public void setUsersname(String usersname) {
        this.usersname = usersname;
    }

    public String getWare() {
        return ware;
    }

    public void setWare(String ware) {
        this.ware = ware;
    }

    public String getNextware() {
        return nextware;
    }

    public void setNextware(String nextware) {
        this.nextware = nextware;
    }

    public Date getIntotime() {
        return intotime;
    }

    public void setIntotime(Date intotime) {
        this.intotime = intotime;
    }

    public Long getSendware() {
        return sendware;
    }

    public void setSendware(Long sendware) {
        this.sendware = sendware;
    }

    public Long getMware() {
        return mware;
    }

    public void setMware(Long mware) {
        this.mware = mware;
    }

    public Long getLastware() {
        return lastware;
    }

    public void setLastware(Long lastware) {
        this.lastware = lastware;
    }

    public String getSendname() {
        return sendname;
    }

    public void setSendname(String sendname) {
        this.sendname = sendname;
    }

    public String getMwaname() {
        return mwaname;
    }

    public void setMwaname(String mwaname) {
        this.mwaname = mwaname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
}
