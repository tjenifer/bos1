package com.bos.modular.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

@TableName("sys_dept")
public class BussnessOrder implements Serializable {
    private static final long serialVersionUID = 1L;

    public Long getOrderid() {
        return orderid;
    }

    public void setOrderid(Long orderid) {
        this.orderid = orderid;
    }

    public String getToaddressdescrript() {
        return toaddressdescrript;
    }

    public void setToaddressdescrript(String toaddressdescrript) {
        this.toaddressdescrript = toaddressdescrript;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public Long getDispactcherid() {
        return dispactcherid;
    }

    public void setDispactcherid(Long dispactcherid) {
        this.dispactcherid = dispactcherid;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getAddressdescrript() {
        return addressdescrript;
    }

    public void setAddressdescrript(String addressdescrript) {
        this.addressdescrript = addressdescrript;
    }

    public String getNumbers() {
        return numbers;
    }

    public void setNumbers(String numbers) {
        this.numbers = numbers;
    }

    public String getGoodsname() {
        return goodsname;
    }

    public void setGoodsname(String goodsname) {
        this.goodsname = goodsname;
    }

    public String getCuname() {
        return cuname;
    }

    public void setCuname(String cuname) {
        this.cuname = cuname;
    }

    public String getCustomerphone() {
        return customerphone;
    }

    public void setCustomerphone(String customerphone) {
        this.customerphone = customerphone;
    }

    public String getToarea() {
        return toarea;
    }

    public void setToarea(String toarea) {
        this.toarea = toarea;
    }

    public String getToprovince() {
        return toprovince;
    }

    public void setToprovince(String toprovince) {
        this.toprovince = toprovince;
    }

    public String getTocity() {
        return tocity;
    }

    public void setTocity(String tocity) {
        this.tocity = tocity;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getToaddress() {
        return toaddress;
    }

    public void setToaddress(String toaddress) {
        this.toaddress = toaddress;
    }

    public String getCustomerid() {
        return customerid;
    }

    public void setCustomerid(String customerid) {
        this.customerid = customerid;
    }

    public String getExamine() {
        return examine;
    }

    public void setExamine(String examine) {
        this.examine = examine;
    }

    public String getDispatchername() {
        return dispatchername;
    }

    public void setDispatchername(String dispatchername) {
        this.dispatchername = dispatchername;
    }

    public Long getDispactcherphone() {
        return dispactcherphone;
    }

    public void setDispactcherphone(Long dispactcherphone) {
        this.dispactcherphone = dispactcherphone;
    }

    public String getToname() {
        return toname;
    }

    public void setToname(String toname) {
        this.toname = toname;
    }

    public String getTonamephone() {
        return tonamephone;
    }

    public void setTonamephone(String tonamephone) {
        this.tonamephone = tonamephone;
    }

    /**
     * 主键
     */
    @TableId(value = "orderid", type = IdType.ID_WORKER)
    private Long orderid;
    /**
     * 到达地址的详细地址
     */

    private String toaddressdescrript;
    /**
     * 备注
     */
    @TableField("remarks")
    private String remarks;
    /**
     * 客户id
     */
    @TableField("customerid")
    private String customerid;
    /**
     * 收件人
     */
    @TableField("toname")
    private String toname;
    /**
     * 收件人电话
     */
    @TableField("tonamephone")
    private String tonamephone;
    /**
     * 区
     */

    private String area;
    /**
     * 省
     */

    private String province;

    /**
     * 市
     */

    private String city;
    /**
     * 到达区
     */

    private String toarea;
    /**
     * 到达省
     */

    private String toprovince;

    /**
     * 到达市
     */

    private String tocity;
    /**
     * 体积
     */
    @TableField("volume")
    private String volume;
    /**
     * 派送员
     */
    @TableField("dispactcherid")
    private Long dispactcherid;
    /**
     * 派送员电话
     */
    @TableField("PHONENUMBER")
    private Long dispactcherphone;
    /**
     * 重量
     */
    @TableField("weight")
    private String weight;
    /**
     * 当前地址的详细地址
     */

    private String addressdescrript;
    /**
     * 件数
     */
    @TableField("numbers")
    private String numbers;

    /**
     * 物品名
     */
    @TableField("goodsname")
    private String goodsname;

    /**
     * 派送员姓名
     */
    @TableField("DISPATCHERNAME")
    private String dispatchername;
    /**
     * 姓名
     */
    @TableField("cuname")
    private String cuname;
    /**
     * 电话号码
     */
    @TableField("customerphone")
    private String customerphone;
    /**
     * 当前地址
     */
    @TableField("address")
    private String address;
    /**
     * 到达地址
     */
    @TableField("toaddress")
    private String toaddress;
    /**
     * 审核状态
     */
    @TableField("examine")
    private String examine;

}
