package com.bos.modular.system.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bos.modular.system.entity.Sendandre;
import com.bos.modular.system.entity.WarehHouse;
import com.bos.modular.system.mapper.WareMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class WareService extends ServiceImpl<WareMapper, WarehHouse> {
    public List<Map<String, Object>> list(String condition) {
        return this.baseMapper.list(condition);
    }
    public List<Map<String, Object>> listbyid(Long condition) {
        return this.baseMapper.listbyid(condition);
    }
    public void addware(WarehHouse warehHouse){this.baseMapper.addware(warehHouse);}

    public void delware(Long condition){ this.baseMapper.delware(condition);}

}
