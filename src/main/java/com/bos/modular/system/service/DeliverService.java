package com.bos.modular.system.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bos.modular.system.entity.Deliver;
import com.bos.modular.system.mapper.DeliverMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class DeliverService extends ServiceImpl<DeliverMapper, Deliver> {
    public List<Map<String, Object>> list(Long condition) {
        return this.baseMapper.list(condition);
    }
    public void deldispa(Long condition){this.baseMapper.deldispa(condition); }
    public void add(Deliver deliver){this.baseMapper.add(deliver);}//添加数据到派送工作表
    public void changeadd(Deliver deliver){this.baseMapper.changeadd(deliver);}//添加数据到派送工作表
    public List<Map<String, Object>> selectbyid(Long condition){return this.baseMapper.selectbyid(condition); }


}
