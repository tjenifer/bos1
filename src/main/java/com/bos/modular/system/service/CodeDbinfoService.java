package com.bos.modular.system.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bos.modular.system.entity.CodeDbinfo;
import com.bos.modular.system.mapper.CodeDbinfoMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 数据库链接信息 服务实现类
 * </p>
 */
@Service
public class CodeDbinfoService extends ServiceImpl<CodeDbinfoMapper, CodeDbinfo> {

}
