package com.bos.modular.system.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bos.core.shiro.ShiroKit;
import com.bos.modular.system.entity.Logicstic;
import com.bos.modular.system.mapper.LogicsticMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class LogicsticService extends ServiceImpl<LogicsticMapper, Logicstic> {
    public List<Map<String, Object>> list(Long condition) {
        return this.baseMapper.list(condition);
    }
    public void addlogicstic(Logicstic logicstic){
        Long userId = ShiroKit.getUserNotNull().getId();
        logicstic.setOperatorid(userId);
        this.baseMapper.addlogicstic(logicstic);
    }
    public void dellogic(Long condition){ this.baseMapper.dellogic(condition);}
    public void diaoupdata(Logicstic logicstic1){ this.baseMapper.diaoupdata(logicstic1);}

}
