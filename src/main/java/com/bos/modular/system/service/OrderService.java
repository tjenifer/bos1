package com.bos.modular.system.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bos.modular.system.entity.BussnessOrder;
import com.bos.modular.system.entity.OrderRoute;
import com.bos.modular.system.mapper.OrderMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class OrderService extends ServiceImpl<OrderMapper, BussnessOrder> {
    public void inserorder(BussnessOrder ord) {
        this.baseMapper.inserorder(ord);
    }
    public void shenhe(Long orderid) {
        this.baseMapper.shenhe(orderid);
    }

    public List<Map<String, Object>> list(Long condition) {
        return this.baseMapper.list(condition);
    }

    public void addroute(OrderRoute ru) {
        this.baseMapper.addroute(ru);
    }
    public void delorder(Long condition){ this.baseMapper.delorder(condition);}

}
