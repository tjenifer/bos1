package com.bos.modular.system.service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Component;
import com.bos.modular.system.entity.dispatcher;
import com.bos.modular.system.mapper.DispatcherMapper;

import java.util.List;
import java.util.Map;

@Component
public class DispathcherService extends ServiceImpl<DispatcherMapper, dispatcher> {

    public List<Map<String, Object>> dislist(String condition) {
        return this.baseMapper.dislist(condition);
    }
    public List<Map<String, Object>> selectlist() {
        return this.baseMapper.selectlist();
    }
    public List<Map<String, Object>> selectall() {
        return this.baseMapper.selectall();
    }
    public void adddis(dispatcher dissss) {
        this.baseMapper.adddis(dissss);
    }
    public void deldis(Long condition){ this.baseMapper.deldis(condition);}
}
