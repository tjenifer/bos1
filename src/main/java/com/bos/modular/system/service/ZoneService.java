package com.bos.modular.system.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.bos.modular.system.entity.Zone;
import com.bos.modular.system.mapper.ZoneMappper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class ZoneService extends ServiceImpl<ZoneMappper, Zone> {
    public List<Map<String, Object>> list(String condition) {
        return this.baseMapper.list(condition);
    }

    public void addzone(Zone zone1){this.baseMapper.addzone(zone1);}
    public void delzone(Long condition){ this.baseMapper.delzone(condition);}

}
