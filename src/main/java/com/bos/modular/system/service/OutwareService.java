package com.bos.modular.system.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bos.modular.system.entity.Outware;
import com.bos.modular.system.mapper.OutwareMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class OutwareService extends ServiceImpl<OutwareMapper, Outware> {
    public List<Map<String, Object>> list(String condition) {
        return this.baseMapper.list(condition);
    }


}
