package com.bos.modular.system.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bos.modular.system.entity.Sendandre;
import com.bos.modular.system.mapper.SendandreMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

//得加个component
@Component
public class sendandreService extends ServiceImpl<SendandreMapper, Sendandre> {
    /**
     * 获取收派列表
     */
    public List<Map<String, Object>> list(String condition) {
        return this.baseMapper.list(condition);
    }
    public List<Map<String, Object>> selectbyid(Long sendid) {
        return this.baseMapper.selectbyid(sendid);
    }
    public void editSendandre(Sendandre send){ this.baseMapper.editSendandre(send);}
    public void addSendandre(Sendandre send){ this.baseMapper.addSendandre(send);}
    public void delsend(Long condition){ this.baseMapper.delsend(condition);}

}
