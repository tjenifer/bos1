package com.bos.modular.system.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bos.modular.system.entity.Region;
import com.bos.modular.system.mapper.RegionMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class RegionService extends ServiceImpl<RegionMapper, Region> {
    public void addregion(Region region){this.baseMapper.addregion(region);}
    public List<Map<String, Object>> list(String condition) {
        return this.baseMapper.list(condition);
    }
    public void delregion(Long condition){ this.baseMapper.delregion(condition);}

}
