package com.bos.modular.system.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bos.modular.system.entity.sign;
import com.bos.modular.system.entity.sign;
import com.bos.modular.system.mapper.SignMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class SignService extends ServiceImpl<SignMapper, sign> {
    public List<Map<String, Object>> list(String condition) {
        return this.baseMapper.list(condition);
    }
    public void addsign(sign signcc){ this.baseMapper.addsign(signcc);}
    public void updatesign(sign signcc){this.baseMapper.updatesign(signcc);}

}
