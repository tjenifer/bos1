package com.bos.modular.system.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bos.modular.system.entity.Fix;
import com.bos.modular.system.mapper.FixMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class FixService extends ServiceImpl<FixMapper, Fix> {
    public List<Map<String, Object>> list(String condition) {
        return this.baseMapper.list(condition);
    }
    public void addfix(Fix fixx){this.baseMapper.addfix(fixx);}
    public void delfix(Long condition){ this.baseMapper.delfix(condition);}


}
