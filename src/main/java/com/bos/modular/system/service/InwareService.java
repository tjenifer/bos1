package com.bos.modular.system.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bos.modular.system.entity.inware;
import com.bos.modular.system.mapper.InwareMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class InwareService extends ServiceImpl<InwareMapper, inware> {
    public List<Map<String, Object>> list(String condition) {
        return this.baseMapper.list(condition);
    }

    public void outwareudata(Long id){ this.baseMapper.outwareudata(id);}

    public void addinware(inware inware22){ this.baseMapper.addinware(inware22);}
    public void outinsert(inware waredd){ this.baseMapper.outinsert(waredd);}

//    public List<Map<String, Object>> outwarelist(String condition) {
//        return this.baseMapper.outwarelist(condition);
//    }


}
