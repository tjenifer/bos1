package com.bos.modular.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bos.modular.system.entity.Region;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface RegionMapper extends BaseMapper<Region> {
    void addregion(Region region);
    List<Map<String, Object>> list(@Param("condition") String condition);
    void delregion (@Param("condition")Long condition);

}
