package com.bos.modular.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bos.modular.system.entity.Deliver;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface DeliverMapper extends BaseMapper<Deliver> {
    /**
     * 获取派件任务列表
     */
    List<Map<String, Object>> list(@Param("condition") Long condition);
    void deldispa(@Param("condition") Long condition);
    void add(Deliver deliver);
    void changeadd(Deliver deliver);

    List<Map<String, Object>> selectbyid(@Param("condition") Long condition);




}
