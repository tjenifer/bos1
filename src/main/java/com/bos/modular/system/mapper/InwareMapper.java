package com.bos.modular.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bos.modular.system.entity.inware;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface InwareMapper extends BaseMapper<inware> {
    List<Map<String, Object>> list(@Param("condition") String condition);

    public void outwareudata(@Param("id") Long id);

    public void addinware(inware inware22);
    public void outinsert(inware waredd);


}
