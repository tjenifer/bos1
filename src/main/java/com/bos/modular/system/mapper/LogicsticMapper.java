package com.bos.modular.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bos.modular.system.entity.Logicstic;

import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface LogicsticMapper extends BaseMapper<Logicstic> {
    /**
     * 获取物流信息列表
     */
    List<Map<String, Object>> list(@Param("condition") Long condition);
    public void addlogicstic(Logicstic logicstic);

    void dellogic (@Param("condition")Long condition);
    void diaoupdata(Logicstic logicstic1);
}

