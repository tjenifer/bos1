package com.bos.modular.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bos.modular.system.entity.WarehHouse;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface WareMapper extends BaseMapper<WarehHouse> {
    List<Map<String, Object>> list(@Param("condition") String condition);
    List<Map<String, Object>> listbyid(@Param("condition") Long condition);
    void addware(WarehHouse warehHouse);
    void delware (@Param("condition")Long condition);

}
