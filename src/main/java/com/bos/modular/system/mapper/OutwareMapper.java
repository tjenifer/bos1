package com.bos.modular.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bos.modular.system.entity.Outware;

import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface OutwareMapper extends BaseMapper<Outware> {
    List<Map<String, Object>> list(@Param("condition") String condition);

}
