package com.bos.modular.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bos.modular.system.entity.CodeDbinfo;

/**
 * <p>
 * 数据库链接信息 Mapper 接口
 * </p>
 */
public interface CodeDbinfoMapper extends BaseMapper<CodeDbinfo> {

}
