package com.bos.modular.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bos.modular.system.entity.dispatcher;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface DispatcherMapper extends BaseMapper<dispatcher> {
    List<Map<String, Object>> dislist(@Param("condition") String condition);
    List<Map<String, Object>> selectlist();
    List<Map<String, Object>> selectall();
    void adddis(dispatcher dissss);
    void deldis (@Param("condition")Long condition);

}
