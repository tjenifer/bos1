package com.bos.modular.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bos.modular.system.entity.Zone;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ZoneMappper extends BaseMapper<Zone> {
    List<Map<String, Object>> list(@Param("condition") String condition);
    void addzone(Zone zone1);

    void delzone (@Param("condition")Long condition);

}
