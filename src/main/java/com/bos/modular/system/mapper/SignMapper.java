package com.bos.modular.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.bos.modular.system.entity.sign;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface SignMapper extends BaseMapper<sign> {
    List<Map<String, Object>> list(@Param("condition") String condition);
    public void addsign(sign signcc);
    public void updatesign(sign signcc);



}
