package com.bos.modular.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bos.modular.system.entity.Fix;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface FixMapper extends BaseMapper<Fix> {
    List<Map<String, Object>> list(@Param("condition") String condition);
    void addfix(Fix fixx);
    void delfix (@Param("condition")Long condition);

}
