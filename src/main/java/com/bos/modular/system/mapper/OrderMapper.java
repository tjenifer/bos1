package com.bos.modular.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bos.modular.system.entity.BussnessOrder;
import com.bos.modular.system.entity.OrderRoute;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


public interface OrderMapper extends BaseMapper<BussnessOrder> {
    void inserorder(BussnessOrder ord);

    void shenhe(@Param("orderid") Long orderid);
    List<Map<String, Object>> list(@Param("condition") Long condition);

    void addroute(OrderRoute ru);
    void delorder (@Param("condition")Long condition);

}
