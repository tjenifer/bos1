package com.bos.modular.system.mapper;

import com.bos.modular.system.entity.Sendandre;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import java.util.Map;

public interface SendandreMapper extends BaseMapper<Sendandre> {
    /**
     * 获取收派列表
     */
    List<Map<String, Object>> list(@Param("condition") String condition);
    /**
     * 根据Id获取收派标准信息
     */
    List<Map<String, Object>> selectbyid(@Param("sendid") Long sendid);

    public void editSendandre(Sendandre send);
    public void addSendandre(Sendandre send);
    public void delsend(@Param("condition") Long condition);
}
