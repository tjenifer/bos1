/**
 * 角色管理的单例
 */
var Fix = {
    id: "Fixtable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    condition: {
        FIXNAME: ""
    }
};

/**
 * 初始化表格的列
 */
Fix.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: 'id', field: 'FIXAREAID', visible: false, align: 'center', valign: 'middle'},
        {title: '定区名称', field: 'FIXNAME', align: 'center', valign: 'middle', sortable: true},
        {title: '负责人', field: 'DISPATCHERNAME', align: 'center', valign: 'middle', sortable: true},
        {title: '负责人号码', field: 'PHONENUMBER', align: 'center', valign: 'middle', sortable: true},
        {title: '所属部门', field: 'FULL_NAME', align: 'center', valign: 'middle', sortable: true}
    ];
};


/**
 * 检查是否选中
 */
Fix.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if (selected.length === 0) {
        Feng.info("请先选中表格中的某一记录！");
        return false;
    } else {
        Fix.seItem = selected[0];
        console.log(Fix.seItem);
        return true;
    }
};

/**
 * 点击添加按钮时
 */
Fix.open = function () {
    this.layerIndex = layer.open({
        type: 2,
        title: '添加定区',
        area: ['800px', '400px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/Fix/fixaddpage'
    });
};

/**
 * 点击修改按钮时
 */
Fix.openChangestan = function () {
    if (this.check()) {

    }
};

/**
 * 删除
 */
Fix.delstan = function () {
    if (this.check()) {

        var operation = function () {
            var ajax = new $ax(Feng.ctxPath + "/Fix/delfix", function () {
                Feng.success("删除成功!");
                Fix.table.refresh();
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("condition", Fix.seItem.FIXAREAID);
            ajax.start();
        };

        Feng.confirm("是否删除定区 " + Fix.seItem.FIXNAME + "?", operation);
    }
};



/**
 * 搜索收派标准
 */
Fix.search = function () {
    var queryData = {};
    queryData['condition'] = Fix.condition.FIXNAME;
    Fix.table.refresh({query: queryData});
};

$(function () {

    Fix.app = new Vue({
        el: '#FixPage',
        data: Fix.condition
    });

    var defaultColunms = Fix.initColumn();
    var table = new BSTable(Fix.id, "/Fix/listt", defaultColunms);
    table.setPaginationType("client");
    table.init();
    Fix.table = table;
});
