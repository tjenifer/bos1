var formdata={};
var zone="";
//验证表单
validateForm=function () {
    // if (formdata.sendname=="") {
    //     return "请输入标准名称";
    // }
    // if (formdata.minwe=="") {
    //     return "请输入最小重量";
    // }
    // if (formdata.maxwe=="") {
    //     return "请输入最大重量";
    // }

    return true;
}
//验证后就提交给controller处理了
addSubmit=function () {
    var ajax = new $ax(Feng.ctxPath + "/Fix/Fixadd", function (data) {
        parent.Feng.success("添加成功!");
        window.parent.Fix.table.refresh();
        closehe();
    }, function (data) {
        parent.Feng.error("添加失败!" + data.responseJSON.message + "!");
    });
    ajax.set(formdata);
    ajax.start();
}
/**
 * 关闭此对话框
 */
closehe = function () {
    // console.log("close");
    var index = parent.layer.getFrameIndex(window.name);
    // console.log(index);
    parent.layer.close(index);
};

ensure = function () {
    // console.log("daodao");

    if (check()) {
        var data = {};
        $("body").find("#FixaddForm").serializeArray().forEach(function (item) {    //获取弹出层写下的数据，input，下拉框啊，之类的表单元素（即changefileform下的所有数据）

            data[item.name] = item.value;   //根据表单元素的name属性来获取数据
        });

        formdata = data;
        formdata["zone"]=zone;
        console.log(formdata);
        var result = validateForm();
        if (result === true) {
            addSubmit();
        } else {
            Feng.alert(result);
        }
    }
};

var Fixadd = {
    id: "FixaddTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    condition: {
        simplecode: ""
    }
};
Fixadd.initColumn = function () {
    return [
        {field: 'selectItem', checkbox: true},
        {title: 'id', field: 'ID', visible: false, align: 'center', valign: 'middle'},
        {title: '关键字', field: 'keyword', align: 'center', valign: 'middle', sortable: true},
        {title: '位置', field: 'sposition', align: 'center', valign: 'middle', sortable: true},
    ];
};
check = function () {
    var selected = $('#' + Fixadd.id).bootstrapTable('getSelections');
    if (selected.length === 0) {
        Feng.info("请先选中表格中的某一记录！");
        return false;
    } else {
        Fixadd.seItem = selected;

        for (var i=0;i<selected.length;i++){
            zone+=selected[i].ID+"|";
        }
        console.log(Fixadd.seItem);
        return true;
    }
};

$(function () {
    var ajax = new $ax(Feng.ctxPath + "/dispatcher/select", function (data) {
        // console.log(data);
        for (var j in data) {
            //alert(data.rows[j].Name);
            // console.log(j);
            $("#selectdispact").append("<option value='" + data[j].id + "'>" + data[j].dispatchername + "</option>");

        }
    });
    ajax.start();
    Fixadd.app = new Vue({
        el: '#FixaddPage',
        data: Fixadd.condition
    });

    var defaultColunms = Fixadd.initColumn();
    var table = new BSTable(Fixadd.id, "/Zone/listt", defaultColunms);
    table.setPaginationType("client");
    table.init();
    Fixadd.table = table;
});