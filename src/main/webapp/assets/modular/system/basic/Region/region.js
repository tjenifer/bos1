/**
 * 角色管理的单例
 */
var Region = {
    id: "regiontable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    condition: {
        simplecode: ""
    }
};

/**
 * 初始化表格的列
 */
Region.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: 'id', field: 'regionid', visible: false, align: 'center', valign: 'middle'},
        {title: '省', field: 'provincee', align: 'center', valign: 'middle', sortable: true},
        {title: '市', field: 'city', align: 'center', valign: 'middle', sortable: true},
        {title: '区', field: 'area', align: 'center', valign: 'middle', sortable: true},
        {title: '邮编', field: 'zipcodee', align: 'center', valign: 'middle', sortable: true},
        {title: '简码', field: 'simplecodee', align: 'center', valign: 'middle', sortable: true},
        {title: '城市编码', field: 'citycodee', align: 'center', valign: 'middle', sortable: true}];
};


/**
 * 检查是否选中
 */
Region.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if (selected.length === 0) {
        Feng.info("请先选中表格中的某一记录！");
        return false;
    } else {
        Region.seItem = selected[0];
        console.log(Region.seItem);
        return true;
    }
};

/**
 * 点击添加按钮时
 */
openpa = function () {
    this.layerIndex = layer.open({
        type: 2,
        title: '添加区域',
        area: ['800px', '400px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/Region/addpage'
    });
};


/**
 * 删除收派标准
 */
delstans = function () {
    if (Region.check()) {

        var operation = function () {
            var ajax = new $ax(Feng.ctxPath + "/Region/delregion", function () {
                Feng.success("删除成功!");
                Region.table.refresh();
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("condition", Region.seItem.regionid);
            ajax.start();
        };

        Feng.confirm("是否删除区域 " + Region.seItem.citycodee + "?", operation);
    }
};



/**
 * 搜索收派标准
 */
Region.search = function () {
    var queryData = {};
    queryData['simplecode'] = Region.condition.simplecode;
    Region.table.refresh({query: queryData});
};

$(function () {

    Region.app = new Vue({
        el: '#regionPage',
        data: Region.condition
    });

    var defaultColunms = Region.initColumn();
    var table = new BSTable(Region.id, "/Region/listt", defaultColunms);
    table.setPaginationType("client");
    table.init();
    Region.table = table;
});
