/**
 * 角色管理的单例
 */
var Dispathcer = {
    id: "DisTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    condition: {
        dispatchername: ""
    }
};

/**
 * 初始化表格的列
 */
Dispathcer.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: 'id', field: 'dispatcherid', visible: false, align: 'center', valign: 'middle'},
        {title: '取派员姓名', field: 'dispatchername', align: 'center', valign: 'middle', sortable: true},
        {title: '电话号码', field: 'phonenumber', align: 'center', valign: 'middle', sortable: true},
        {title: '是否有PDA', field: 'has_PDA', align: 'center', valign: 'middle', sortable: true},
        {title: '是否作废', field: 'useornot', align: 'center', valign: 'middle', sortable: true},
        {title: '取派标准', field: 'sendname', align: 'center', valign: 'middle', sortable: true},
        {title: '部门', field: 'deptname', align: 'center', valign: 'middle', sortable: true}];
};
deldis=function () {
    if (Dispathcer.check()) {

        var operation = function () {
            var ajax = new $ax(Feng.ctxPath + "/dispatcher/deldis", function () {
                Feng.success("删除成功!");
                Dispathcer.table.refresh();
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("condition", Dispathcer.seItem.dispatcherid);
            ajax.start();
        };

        Feng.confirm("是否删除取派员 " + Dispathcer.seItem.dispatchername
            + "?", operation);
    }

}


/**
 * 检查是否选中
 */
Dispathcer.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if (selected.length === 0) {
        Feng.info("请先选中表格中的某一记录！");
        return false;
    } else {
        Dispathcer.seItem = selected[0];
        console.log(Dispathcer.seItem);
        return true;
    }
};



/**
 * 点击添加按钮时
 */
openDiss = function () {
    this.layerIndex = layer.open({
        type: 2,
        title: '添加收派员',
        area: ['800px', '400px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/dispatcher/disaddpage'
    });
};


/**
 * 搜索收派标准
 */
Dispathcer.search = function () {
    var queryData = {};
    queryData['dispatchername'] = Dispathcer.condition.dispatchername;
    Dispathcer.table.refresh({query: queryData});
};

$(function () {

    Dispathcer.app = new Vue({
        el: '#DispatcherPage',
        data: Dispathcer.condition
    });

    var defaultColunms = Dispathcer.initColumn();
    var table = new BSTable(Dispathcer.id, "/dispatcher/listt", defaultColunms);
    table.setPaginationType("client");
    table.init();
    Dispathcer.table = table;
});
