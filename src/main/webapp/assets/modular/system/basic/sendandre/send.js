/**
 * 角色管理的单例
 */
var Sendandre = {
    id: "sendTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    condition: {
        sendName: ""
    }
};

/**
 * 初始化表格的列
 */
Sendandre.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: 'id', field: 'sendid', visible: false, align: 'center', valign: 'middle'},
        {title: '标准名称', field: 'sendname', align: 'center', valign: 'middle', sortable: true},
        {title: '最小重量', field: 'minwe', align: 'center', valign: 'middle', sortable: true},
        {title: '最大重量', field: 'maxwe', align: 'center', valign: 'middle', sortable: true},
        {title: '操作人', field: 'opername', align: 'center', valign: 'middle', sortable: true},
        {title: '操作时间', field: 'dotime', align: 'center', valign: 'middle', sortable: true},
        {title: '操作单位', field: 'DEPTNAME', align: 'center', valign: 'middle', sortable: true}];
};


/**
 * 检查是否选中
 */
Sendandre.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if (selected.length === 0) {
        Feng.info("请先选中表格中的某一记录！");
        return false;
    } else {
        Sendandre.seItem = selected[0];
        console.log(Sendandre.seItem);
        return true;
    }
};

/**
 * 点击添加按钮时
 */
Sendandre.open = function () {
    this.layerIndex = layer.open({
        type: 2,
        title: '添加收派标准',
        area: ['800px', '400px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/sendandreceive/sadd'
    });
};

/**
 * 点击修改按钮时
 */
Sendandre.openChangestan = function () {
    if (this.check()) {
        this.layerIndex = layer.open({
            type: 2,
            title: '修改收派标准',
            area: ['800px', '400px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/sendandreceive/stander_edit?sendid=' + this.seItem.sendid,
            success: function (layero, index) {    //成功获得加载changefile.html时
                //// console.log(obj.data.editAble);
                var body = layer.getChildFrame('body', index);
                //console.log(rowselect[0].filename);
                body.find(".sendid").val(Sendandre.seItem.sendid);
                body.find(".sendname").val(Sendandre.seItem.sendname);   //通过class名进行获取数据
                body.find(".minwe").val(Sendandre.seItem.minwe);
                body.find(".maxwe").val(Sendandre.seItem.maxwe);
            }
        });
    }
};

/**
 * 删除收派标准
 */
Sendandre.delstan = function () {
    if (this.check()) {

        var operation = function () {
            var ajax = new $ax(Feng.ctxPath + "/sendandreceive/delsend", function () {
                Feng.success("删除成功!");
                Sendandre.table.refresh();
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("sendid", Sendandre.seItem.sendid);
            ajax.start();
        };

        Feng.confirm("是否删除标准 " + Sendandre.seItem.sendname
            + "?", operation);
    }
};



/**
 * 搜索收派标准
 */
Sendandre.search = function () {
    var queryData = {};
    queryData['sendName'] = Sendandre.condition.sendName;
    Sendandre.table.refresh({query: queryData});
};

$(function () {

    Sendandre.app = new Vue({
        el: '#rolePage',
        data: Sendandre.condition
    });

    var defaultColunms = Sendandre.initColumn();
    var table = new BSTable(Sendandre.id, "/sendandreceive/listt", defaultColunms);
    table.setPaginationType("client");
    table.init();
    Sendandre.table = table;
});
