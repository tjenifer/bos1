/**
 * 角色管理的单例
 */
var Zone = {
    id: "zonetable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    condition: {
        keyword: ""
    }
};

/**
 * 初始化表格的列
 */
Zone.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: 'id', field: 'zoneid', visible: false, align: 'center', valign: 'middle'},
        {title: '省', field: 'PROVINCE', align: 'center', valign: 'middle', sortable: true},
        {title: '市', field: 'CITY', align: 'center', valign: 'middle', sortable: true},
        {title: '区', field: 'AREA', align: 'center', valign: 'middle', sortable: true},
        {title: '分拣编码', field: 'sortingcode', align: 'center', valign: 'middle', sortable: true},
        {title: '关键字', field: 'keyword', align: 'center', valign: 'middle', sortable: true},
        {title: '起始号', field: 'startingnumber', align: 'center', valign: 'middle', sortable: true},
        {title: '终止号', field: 'endnumber', align: 'center', valign: 'middle', sortable: true},
        {title: '单双号', field: 'oddoreven', align: 'center', valign: 'middle', sortable: true},
        {title: '位置信息', field: 'sposition', align: 'center', valign: 'middle', sortable: true}
    ];
};


/**
 * 检查是否选中
 */
Zone.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if (selected.length === 0) {
        Feng.info("请先选中表格中的某一记录！");
        return false;
    } else {
        Zone.seItem = selected[0];
        console.log(Zone.seItem);
        return true;
    }
};

/**
 * 点击添加按钮时
 */
openhh = function () {
        this.layerIndex = layer.open({
            type: 2,
            title: '添加分区',
            area: ['800px', '400px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/Zone/addpage'
        });
};



/**
 * 删除收派标准
 */
Zone.delstan = function () {
    if (this.check()) {

        var operation = function () {
            var ajax = new $ax(Feng.ctxPath + "/Zone/delzone", function () {
                Feng.success("删除成功!");
                Zone.table.refresh();
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("condition", Zone.seItem.ID);
            ajax.start();
        };

        Feng.confirm("是否删除标准 " + Zone.seItem.keyword + "?", operation);
    }
};



/**
 * 搜索
 */
Zone.search = function () {
    var queryData = {};
    queryData['keyword'] = Zone.condition.keyword;
    Zone.table.refresh({query: queryData});
};

$(function () {

    Zone.app = new Vue({
        el: '#zonenPage',
        data: Zone.condition
    });

    var defaultColunms = Zone.initColumn();
    var table = new BSTable(Zone.id, "/Zone/listt", defaultColunms);
    table.setPaginationType("client");
    table.init();
    Zone.table = table;
});
