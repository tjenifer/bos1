var frodata={};
/**
 * 角色管理的单例
 */
var sign = {
    id: "signTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    condition: {
        orderid: ""
    }
};

/**
 * 初始化表格的列
 */
sign.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: 'id', field: 'id', visible: false, align: 'center', valign: 'middle'},
        {title: '订单号', field: 'orderid', align: 'center', valign: 'middle', sortable: true},
        {title: '派件人id', field: 'userid', align: 'center', valign: 'middle', sortable: true},
        {title: '派件人', field: 'delivername', align: 'center', valign: 'middle', sortable: true},
        {title: '派件人号码', field: 'deliverphone', align: 'center', valign: 'middle', sortable: true},
        {title: '图片路径', field: 'imagepath', align: 'center', valign: 'middle', sortable: true},
        {title: '签收时间', field: 'signtime', align: 'center', valign: 'middle', sortable: true}];
};

change=function () {
    if (check()) {
        this.layerIndex = layer.open({
            type: 2,
            title: '签收',
            area: ['800px', '400px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/signli/signadd',
            success: function (layero, index) {    //成功获得加载changefile.html时
                //// console.log(obj.data.editAble);
                var body = layer.getChildFrame('body', index);
                //console.log(rowselect[0].filename);
                body.find(".orderid").val(sign.seItem.orderid);

            }
        });
    }

}
/**
 * 检查是否选中
 */
check = function () {
    var selected = $('#' + sign.id).bootstrapTable('getSelections');
    if (selected.length === 0) {
        Feng.info("请先选中表格中的某一记录！");
        return false;
    } else {
        sign.seItem = selected[0];
        console.log(sign.seItem);
        frodata=sign.seItem;
        return true;
    }
};


/**
 * 搜索
 */
search = function () {
    var queryData = {};
    queryData['orderid'] = sign.condition.orderid;
    sign.table.refresh({query: queryData});
};


$(function () {

    sign.app = new Vue({
        el: '#signPage',
        data: sign.condition
    });

    var defaultColunms = sign.initColumn();
    var table = new BSTable(sign.id, "/signli/listt", defaultColunms);
    table.setPaginationType("client");
    table.init();
    sign.table = table;
});
