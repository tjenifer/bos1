var frodata={};
/**
 * 角色管理的单例
 */
var deliver = {
    id: "deliverTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    condition: {
        orderid: ""
    }
};

/**
 * 初始化表格的列
 */
deliver.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: 'id', field: 'id', visible: false, align: 'center', valign: 'middle'},
        {title: '订单号', field: 'orderid', align: 'center', valign: 'middle', sortable: true},
        {title: '物品名', field: 'goodsname', align: 'center', valign: 'middle', sortable: true},
        {title: '派件地址', field: 'positions', align: 'center', valign: 'middle', sortable: true},
        {title: '收件人', field: 'signname', align: 'center', valign: 'middle', sortable: true},
        {title: '收件人号码', field: 'signphone', align: 'center', valign: 'middle', sortable: true},
        {title: '派送人', field: 'delivername', align: 'center', valign: 'middle', sortable: true},
        {title: '派送人电话', field: 'deliverphone', align: 'center', valign: 'middle', sortable: true}];
};


/**
 * 检查是否选中
 */
check = function () {
    var selected = $('#' + deliver.id).bootstrapTable('getSelections');
    if (selected.length === 0) {
        Feng.info("请先选中表格中的某一记录！");
        return false;
    } else {
        deliver.seItem = selected[0];
        console.log(deliver.seItem);
        frodata=deliver.seItem;
        return true;
    }
};


/**
 * 搜索
 */
search = function () {
    var queryData = {};
    queryData['orderid'] = deliver.condition.orderid;
    deliver.table.refresh({query: queryData});
};
//签收
sign = function () {
    if (check()) {

        var ajax = new $ax(Feng.ctxPath + "/deliver/sign", function (data) {
            parent.Feng.success("已签收!");
            deliver.table.refresh();
            closehe();
        }, function (data) {
            parent.Feng.error("签收失败!" + data.responseJSON.message + "!");
        });
        ajax.set(frodata);
        ajax.start();
    }
};

change=function () {
    if (check()) {
        this.layerIndex = layer.open({
            type: 2,
            title: '调度',
            area: ['800px', '400px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/deliver/change',
            success: function (layero, index) {    //成功获得加载changefile.html时
                //// console.log(obj.data.editAble);
                var body = layer.getChildFrame('body', index);
                //console.log(rowselect[0].filename);
                body.find(".orderid").val(deliver.seItem.orderid);

            }
        });

    }
}

$(function () {

    deliver.app = new Vue({
        el: '#deliverPage',
        data: deliver.condition
    });

    var defaultColunms = deliver.initColumn();
    var table = new BSTable(deliver.id, "/deliver/listt", defaultColunms);
    table.setPaginationType("client");
    table.init();
    deliver.table = table;
});
