/**
 * 角色管理的单例
 */
var logicstic = {
    id: "logicsticTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    condition: {
        orderid: ""
    }
};

/**
 * 初始化表格的列
 */
logicstic.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: 'id', field: 'id', visible: false, align: 'center', valign: 'middle'},
        {title: '订单号', field: 'orderid', align: 'center', valign: 'middle', sortable: true},
        {title: '事件名', field: 'eventt', align: 'center', valign: 'middle', sortable: true},
        {title: '操作人', field: 'username', align: 'center', valign: 'middle', sortable: true},
        {title: '操作时间', field: 'eventtime', align: 'center', valign: 'middle', sortable: true},
        {title: '下一站', field: 'nextdo', align: 'center', valign: 'middle', sortable: true},
        {title: '订单状态', field: 'nowstate', align: 'center', valign: 'middle', sortable: true},

        ];
};


/**
 * 检查是否选中
 */
logicstic.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if (selected.length === 0) {
        Feng.info("请先选中表格中的某一记录！");
        return false;
    } else {
        logicstic.seItem = selected[0];
        console.log(logicstic.seItem);
        return true;
    }
};


/**
 * 点击修改按钮时
 */
logicstic.openChangestan = function () {
    if (this.check()) {
        this.layerIndex = layer.open({
            type: 2,
            title: '修改收派标准',
            area: ['800px', '400px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/sendandreceive/stander_edit?sendid=' + this.seItem.sendid,
            success: function (layero, index) {    //成功获得加载changefile.html时
                //// console.log(obj.data.editAble);
                var body = layer.getChildFrame('body', index);
                //console.log(rowselect[0].filename);
                body.find(".sendid").val(Sendandre.seItem.sendid);
                body.find(".sendname").val(Sendandre.seItem.sendname);   //通过class名进行获取数据
                body.find(".minwe").val(Sendandre.seItem.minwe);
                body.find(".maxwe").val(Sendandre.seItem.maxwe);
            }
        });
    }
};

/**
 * 删除
 */
logicstic.delstan = function () {
    if (this.check()) {

        var operation = function () {
            var ajax = new $ax(Feng.ctxPath + "/logistic/dellogic", function () {
                Feng.success("删除成功!");
                logicstic.table.refresh();
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("condition", logicstic.seItem.id);
            ajax.start();
        };

        Feng.confirm("是否删除 " + logicstic.seItem.eventt + "?", operation);
    }
};



/**
 * 搜索收派标准
 */
logicstic.search = function () {
    var queryData = {};
    queryData['orderid'] = logicstic.condition.orderid;
    logicstic.table.refresh({query: queryData});
};

$(function () {

    logicstic.app = new Vue({
        el: '#logicsticPage',
        data: logicstic.condition
    });

    var defaultColunms = logicstic.initColumn();
    var table = new BSTable(logicstic.id, "/logistic/listt", defaultColunms);
    table.setPaginationType("client");
    table.init();
    logicstic.table = table;
});
