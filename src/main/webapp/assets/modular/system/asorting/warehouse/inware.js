var fdata={};
/**
 * 角色管理的单例
 */
var inware = {
    id: "inwareTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    condition: {
        ware: ""
    }
};

/**
 * 初始化表格的列
 */
inware.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: 'id', field: 'id', visible: false, align: 'center', valign: 'middle'},
        {title: '订单号', field: 'orderid', align: 'center', valign: 'middle', sortable: true},
        {title: '操作人', field: 'usersname', align: 'center', valign: 'middle', sortable: true},
        {title: '当前仓库', field: 'ware', align: 'center', valign: 'middle', sortable: true},
        {title: '下一站', field: 'nextware', align: 'center', valign: 'middle', sortable: true},
        {title: '入库时间', field: 'intotime', align: 'center', valign: 'middle', sortable: true}

    ];
};


/**
 * 检查是否选中
 */
inware.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if (selected.length === 0) {
        Feng.info("请先选中表格中的某一记录！");
        return false;
    } else {
        inware.seItem = selected[0];
        console.log(inware.seItem);
        fdata["warehouseid"]=inware.seItem.warehouseid;
        fdata["orderid"]=inware.seItem.orderid;
        fdata["id"]=inware.seItem.id;
        fdata["nextware"]=inware.seItem.nextware;
        fdata["ware"]=inware.seItem.ware;
        console.log(fdata);
        return true;
    }
};

/**
 * 点击入库按钮时
 */
inware.open = function () {
    this.layerIndex = layer.open({
        type: 2,
        title: '入库',
        area: ['800px', '400px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/inware/add'
    });
};

/**
 * 点击出库按钮时
 */
inware.openChangestan = function () {
    if (this.check()) {
        var ajax = new $ax(Feng.ctxPath + "/inware/outwareudata", function () {
            Feng.success("出库成功!");
            inware.table.refresh();
        }, function (data) {
            Feng.error("出库失败!" + data.responseJSON.message + "!");
        });
        console.log(fdata);
        ajax.set(fdata);
        // ajax.set("id", inware.seItem.id);
        ajax.start();
    }
};




/**
 * 搜索收派标准
 */
inware.search = function () {
    var queryData = {};
    queryData['ware'] = inware.condition.ware;
    inware.table.refresh({query: queryData});
};

$(function () {

    inware.app = new Vue({
        el: '#inwarePage',
        data: inware.condition
    });

    var defaultColunms = inware.initColumn();
    var table = new BSTable(inware.id, "/inware/listt", defaultColunms);
    table.setPaginationType("client");
    table.init();
    inware.table = table;
});
