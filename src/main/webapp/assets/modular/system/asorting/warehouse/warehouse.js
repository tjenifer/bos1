/**
 * 角色管理的单例
 */
var ware = {
    id: "wareTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    condition: {
        warename: ""
    }
};

/**
 * 初始化表格的列
 */
ware.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: 'id', field: 'wareid', visible: false, align: 'center', valign: 'middle'},
        {title: '仓库名', field: 'warename', align: 'center', valign: 'middle', sortable: true},
        {title: '省', field: 'province', align: 'center', valign: 'middle', sortable: true},
        {title: '市', field: 'city', align: 'center', valign: 'middle', sortable: true},
        {title: '区', field: 'zones', align: 'center', valign: 'middle', sortable: true}
       ];
};


/**
 * 检查是否选中
 */
ware.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if (selected.length === 0) {
        Feng.info("请先选中表格中的某一记录！");
        return false;
    } else {
        ware.seItem = selected[0];
        console.log(ware.seItem);
        return true;
    }
};

/**
 * 点击添加
 */
ware.open = function () {
    this.layerIndex = layer.open({
        type: 2,
        title: '添加仓库',
        area: ['800px', '400px'], //宽高
        fix: false, //不固定
        maxmin: true,
        content: Feng.ctxPath + '/sort/addpage'
    });
};


/**
 * 删除
 */
ware.delstan = function () {
    if (this.check()) {

        var operation = function () {
            var ajax = new $ax(Feng.ctxPath + "/sort/delware", function () {
                Feng.success("删除成功!");
                ware.table.refresh();
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("condition", ware.seItem.wareid);
            ajax.start();
        };

        Feng.confirm("是否删除仓库 " + ware.seItem.warename + "?", operation);
    }
};



/**
 * 搜索收派标准
 */
ware.search = function () {
    var queryData = {};
    queryData['warename'] = ware.condition.warename;
    ware.table.refresh({query: queryData});
};

$(function () {

    ware.app = new Vue({
        el: '#warePage',
        data: ware.condition
    });

    var defaultColunms = ware.initColumn();
    var table = new BSTable(ware.id, "/sort/listt", defaultColunms);
    table.setPaginationType("client");
    table.init();
    ware.table = table;
});
