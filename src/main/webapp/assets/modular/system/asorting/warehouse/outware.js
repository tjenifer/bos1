var fromdata={};
/**
 * 角色管理的单例
 */
var inware = {
    id: "inwareTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    condition: {
        ware: ""
    }
};

/**
 * 初始化表格的列
 */
inware.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: 'id', field: 'id', visible: false, align: 'center', valign: 'middle'},
        {title: '订单号', field: 'orderid', align: 'center', valign: 'middle', sortable: true},
        {title: '操作人', field: 'username', align: 'center', valign: 'middle', sortable: true},
        {title: '出库仓库', field: 'warename', align: 'center', valign: 'middle', sortable: true},
        // {title: '下一站', field: 'nextware', align: 'center', valign: 'middle', sortable: true},
        {title: '出库时间', field: 'ootime', align: 'center', valign: 'middle', sortable: true}
    ];
};


/**
 * 检查是否选中
 */
inware.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if (selected.length === 0) {
        Feng.info("请先选中表格中的某一记录！");
        return false;
    } else {
        inware.seItem = selected[0];
        console.log(inware.seItem);
        return true;
    }
};






/**
 * 搜索收派标准
 */
inware.search = function () {
    inware.check();
    // var queryData = {};
    // queryData['ware'] = inware.condition.ware;
    // inware.table.refresh({query: queryData});
};

$(function () {

    inware.app = new Vue({
        el: '#inwarePage',
        data: inware.condition
    });

    var defaultColunms = inware.initColumn();
    var table = new BSTable(inware.id, "/outware/outwarelists", defaultColunms);
    table.setPaginationType("client");
    table.init();
    inware.table = table;
});
