/**
 * 角色管理的单例
 */
var order = {
    id: "orderTable",	//表格id
    seItem: null,		//选中的条目
    table: null,
    layerIndex: -1,
    condition: {
        orderid: ""
    }
};

/**
 * 初始化表格的列
 */
order.initColumn = function () {
    return [
        {field: 'selectItem', radio: true},
        {title: '单号', field: 'orderid', align: 'center', valign: 'middle', sortable: true},
        {title: '客户', field: 'cuname', align: 'center', valign: 'middle', sortable: true},
        {title: '客户电话', field: 'customerphone', align: 'center', valign: 'middle', sortable: true},
        {title: '物品名', field: 'goodsname', align: 'center', valign: 'middle', sortable: true},
        {title: '取件地点', field: 'address', align: 'center', valign: 'middle', sortable: true},
        {title: '到达地址', field: 'toaddress', align: 'center', valign: 'middle', sortable: true},
        {title: '取件人', field: 'DISPATCHERNAME', align: 'center', valign: 'middle', sortable: true},
        {title: '派送员电话', field: 'PHONENUMBER', align: 'center', valign: 'middle', sortable: true},
        {title: '审核状态', field: 'examine', align: 'center', valign: 'middle', sortable: true}];
};


/**
 * 检查是否选中
 */
order.check = function () {
    var selected = $('#' + this.id).bootstrapTable('getSelections');
    if (selected.length === 0) {
        Feng.info("请先选中表格中的某一记录！");
        return false;
    } else {
        order.seItem = selected[0];
        console.log(order.seItem);
        return true;
    }
};
//路线分配
function giveroute() {
    if (order.check()) {
        this.layerIndex = layer.open({
            type: 2,
            title: '分配路线',
            area: ['800px', '400px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: Feng.ctxPath + '/order/routepage',
            success: function (layero, index) {    //成功获得加载changefile.html时
                //// console.log(obj.data.editAble);
                var body = layer.getChildFrame('body', index);
                //console.log(rowselect[0].filename);
                body.find(".orderid").val(order.seItem.orderid);

            }
        })
    }


}
//审核订单
function shenhe() {
    // console.log("进来了");
    if (order.check()) {
        var ajax = new $ax(Feng.ctxPath + "/order/shenhe", function () {
            Feng.success("审核成功!");
            order.table.refresh();
        }, function (data) {
            Feng.error("审核失败!" + data.responseJSON.message + "!");
        });
        ajax.set("orderid", order.seItem.orderid);
        ajax.start();
    }
};



/**
 * 删除
 */
delstan = function () {
    if (order.check()) {

        var operation = function () {
            var ajax = new $ax(Feng.ctxPath + "/order/delorder", function () {
                Feng.success("删除成功!");
                order.table.refresh();
            }, function (data) {
                Feng.error("删除失败!" + data.responseJSON.message + "!");
            });
            ajax.set("condition", order.seItem.orderid);
            ajax.start();
        };

        Feng.confirm("是否删除 " + order.seItem.goodsname + "?", operation);
    }
};



/**
 * 搜索收派标准
 */
order.search = function () {
    var queryData = {};
    queryData['condition'] = order.condition.orderid;
    order.table.refresh({query: queryData});
};

$(function () {

    order.app = new Vue({
        el: '#orderPage',
        data: order.condition
    });

    var defaultColunms = order.initColumn();
    var table = new BSTable(order.id, "/order/listt", defaultColunms);
    table.setPaginationType("client");
    table.init();
    order.table = table;
});
